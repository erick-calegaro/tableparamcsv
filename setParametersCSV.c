
int saveParameters(void)
{

#ifndef APPLY_PARAMETERS
    return RC_OK;
#endif

    char string_data [1024 * 100];
    char table       [1024 * 30];
    char array       [1024 * 15];
    char insert      [1024 * 2];
    char where       [128 + 1];
    char array_cod   [128 + 1];
    char sAux        [128 + 1];
    char zAux        [128 + 1];
    char product     [3 + 1];
    char nro_cod     [2 + 1];
    char id_aquirer  [2 + 1];

    int contador = 0;
    float progresso = 0;

    int  rc         = RC_OK, i = 0, tabela = 1, tamanho = 0, num_parameters = 0, last_pos = 0;
    int  tabela0    = 0, tabela1 = 0, tabela2 = 0, tabela3 = 0, tabela4 = 0, tabela5 = 0, tabela6 = 0, tabela7 = 0;
    uint offset     = 0, ptrArray = 0;

    FILE *PARAM_FILE  = NULL;
    FILE *AUX_FILE    = NULL;
    char *resultado   = NULL;

    // limpa as variaveis
    memset(string_data, 0, sizeof(string_data));

#ifdef SHOW_PROGRESS
        struct timeval inicio, fim;
        gettimeofday(&inicio, NULL);
        printf("SALVANDO PARAMETROS EM CSV\n\n");
#endif
    
    // abre o PARAM_FILE texto
    PARAM_FILE = fopen (PARAM_TABLES_PERM, "rt");
    if (PARAM_FILE == NULL)
    {
        printf("Failed to open file '%s'\n", PARAM_TABLES_PERM);
        return RC_ERR;
    }

    resultado = fgets (string_data, sizeof (string_data), PARAM_FILE);

    if (resultado == NULL)
    {
        printf(">>> Erro ao carregar o PARAM_FILE param_tables.dat <<<\n");
        fclose(PARAM_FILE);
        rc = -1;
    }
    tamanho = strlen(string_data);
    //printf("string_data [%d] : %s\n", tamanho, string_data);

    for (i = 0; i < tamanho; i++)
    {
    
        if(string_data[i] == '\\')
        {
            switch(tabela)
            {
                case 1: tabela1 = i;    tabela++;       break;
                case 2: tabela2 = i;    tabela++;       break;
                case 3: tabela3 = i;    tabela++;       break;
                case 4: tabela4 = i;    tabela++;       break;
                case 5: tabela5 = i;    tabela++;       break;
                case 6: tabela6 = i;    tabela++;       break;
                case 7: tabela7 = i;    tabela++;       break;
                case 8: tabela = 0;         break;
                default:
                break;
            }

        }

    }

#ifdef PARAM_DEBUG
    printf("Flag tabela0   [%d]\n"    , tabela0);
    printf("Flag tabela1   [%d]\n"    , tabela1);
    printf("Flag tabela2   [%d]\n"    , tabela2);
    printf("Flag tabela3   [%d]\n"    , tabela3);
    printf("Flag tabela4   [%d]\n"    , tabela4);
    printf("Flag tabela5   [%d]\n"    , tabela5);
    printf("Flag tabela6   [%d]\n"    , tabela6);
    printf("Flag tabela7   [%d]\n"    , tabela7);
    printf("Flag tamanho   [%d]\n"    , tamanho);
#endif
 
    //Salva tabela timestamp
    memset(table, 0x00, sizeof(table));
    strncpy(table, &string_data[tabela0], 14);
    //utl_Config_ActualizeDateTime(table);

#ifdef SHOW_PROGRESS
        system("tput cuu1; tput dl1");
            printf("ATUALIZANDO DATA E HORA %d%%                         \n", 100);
#endif
#ifdef PARAM_DEBUG
    printf("Data e hora atualizados para [%s]\n", table);
#endif

    //limpa a tabela acquirer_table
    remove(CSV_TABLE_ACQUIRER_TABLE);

    //Salva tabela acquirer_table
        memset(table, 0x00, sizeof(table));
        strncpy(table, &string_data[tabela1+1], tabela2 - tabela1);

#ifdef SHOW_PROGRESS
            system("tput cuu1; tput dl1");
            printf("SALVANDO ADQUIRENTE %d%%                         \n", 100);
#endif
#ifdef PARAM_DEBUG
        printf("ID_acquirer = %s\n"   , "1");
        printf("adiq_code   = %.03s\n", &table[offset]);
        printf("name        = %.20s\n", &table[offset+3]);
#endif

        //Da o insert na tabela acquirer_table
            memset(insert, 0x00, sizeof(insert));
            sprintf(insert, "1;%.03s;%.10s", &table[offset], &table[offset+3]);
            rc = utl_CSV_insertLine(CSV_TABLE_ACQUIRER_TABLE, insert);
    
#ifdef PARAM_DEBUG
            printf("Foi inserida a linha = %s\n", insert);
#endif


    //Busca a primary key do adiquirente para relacionar com demais tabelas
    id_aquirer[0] = '1'; 

    //limpa a tabela ec_data
    remove(CSV_TABLE_EC_DATA);

    //Salva tabela ec_data
        memset(table, 0x00, sizeof(table));
        strncpy(table, &string_data[tabela2+1], tabela3 - tabela2);

#ifdef SHOW_PROGRESS
            system("tput cuu1; tput dl1");
            printf("SALVANDO EC %d%%                         \n", 100);
#endif
#ifdef PARAM_DEBUG
        printf("name              = %.38s\n", &table[offset]);
        printf("address_1         = %.38s\n", &table[offset+38]);
        printf("address_2         = %.38s\n", &table[offset+76]);
        printf("simbol            = %.04s\n", &table[offset+114]);
        printf("currency_code     = %.03s\n", &table[offset+118]);
        printf("currency_exponent = %.01s\n", &table[offset+121]);
        printf("country_code      = %.03s\n", &table[offset+122]);
        printf("category_code     = %.04s\n", &table[offset+125]);
        printf("byte_1            = %.02s\n", &table[offset+129]);
        printf("byte_2            = %.02s\n", &table[offset+131]);
        printf("byte_3            = %.02s\n", &table[offset+133]);        
        printf("ID_acquirer       = %s\n"   , id_aquirer);
#endif
    

        //Da o insert na tabela ec_data
        memset(insert, 0x00, sizeof(insert));
        sprintf(insert, "%.38s;%.38s;%.38s;%.04s;%.03s;%.01s;%.03s;%.04s;%.02s;%.02s;%.02s;%.02s", 
            &table[offset]    , &table[offset+38] , &table[offset+76] , &table[offset+114], &table[offset+118], &table[offset+121],
            &table[offset+122], &table[offset+125], &table[offset+129], &table[offset+131], &table[offset+133], id_aquirer);
        rc = utl_CSV_insertLine(CSV_TABLE_EC_DATA,  insert);

#ifdef PARAM_DEBUG
            printf("Foi inserida a linha = %s\n", insert);
#endif

    //limpa a tabela bin_table
    remove(CSV_TABLE_BIN_TABLE);
    
    //Salva tabela bin_table
        memset(table, 0x00, sizeof(table));
        strncpy(table, &string_data[tabela3+1], tabela4 - tabela3);
        contador = strlen(table);

        for (offset = 0; offset < (strlen(table) - 1); offset+=29)
        {

            progresso = (float)offset / ((float)contador / 100);
#ifdef SHOW_PROGRESS
                system("tput cuu1; tput dl1");
            printf("SALVANDO BINS %d%%                         \n", (int)progresso);
#endif

#ifdef PARAM_DEBUG
                printf("ID_acquirer = %s\n"   , id_aquirer);
                printf("bin_initial = %.10s\n", &table[offset]);
                printf("bin_final   = %.10s\n", &table[offset+10]);
                printf("cod_product = %.03s\n", &table[offset+20]);
                printf("byte_1      = %.02s\n", &table[offset+23]);
                printf("byte_2      = %.02s\n", &table[offset+25]);
                printf("byte_3      = %.02s\n", &table[offset+27]);
#endif

        //Da o insert na tabela bin_table
            memset(insert, 0x00, sizeof(insert));
            sprintf(insert, "%s;%.10s;%.10s;%.03s;%.02s;%.02s;%.02s", 
                id_aquirer, &table[offset],     &table[offset+10], &table[offset+20], 
                            &table[offset+23],  &table[offset+25], &table[offset+27]);
            rc = utl_CSV_insertLine(CSV_TABLE_BIN_TABLE, insert);
#ifdef PARAM_DEBUG
            printf("Foi inserida a linha = %s\n", insert);
#endif
        }

    //limpa a tabela products_table
    remove(CSV_TABLE_PRODUCTS_TABLE);

    //Salva tabela products_table
    memset(table, 0x00, sizeof(table));
    strncpy(table, &string_data[tabela4+1], tabela5 - tabela4);
    contador = strlen(table);

    for (offset = 0; offset < (strlen(table) - 1); offset+=38)
    {
        progresso = (float)offset / ((float)contador / 100);

        //gui_busy_start (GUI_BUSY_PARAM_PRODUCTS+(int)progresso, FALSE, NULL);
#ifdef SHOW_PROGRESS
            system("tput cuu1; tput dl1");
            printf("SALVANDO PRODUTOS %d%%                         \n", (int)progresso);
#endif
#ifdef PARAM_DEBUG
            printf("cod_product      = %.03s\n", &table[offset]);
            printf("name_product     = %.20s\n", &table[offset+3]);
            printf("type_product     = %.02s\n", &table[offset+23]);
            printf("byte_1           = %.02s\n", &table[offset+25]);
            printf("byte_2           = %.02s\n", &table[offset+27]);
            printf("byte_3           = %.02s\n", &table[offset+29]);
            printf("pan_mask_initial = %.02s\n", &table[offset+31]);
            printf("pan_mask_final   = %.02s\n", &table[offset+33]);
            printf("cod_funcional    = %.03s\n", &table[offset+35]);
            printf("cod_AID          = %.02s\n", "");             
            printf("ID_acquirer      = %s\n"  , id_aquirer);
#endif

        //Da o insert na tabela products_table
        memset(insert, 0x00, sizeof(insert));
        sprintf(insert, "%.03s;%.20s;%.02s;%.02s;%.02s;%.02s;%.02s;%.02s;%.03s;%.02s;%.02s", 
            &table[offset], &table[offset+3], &table[offset+23], &table[offset+25], &table[offset+27], &table[offset+29],
            &table[offset+31], &table[offset+33], &table[offset+35], "", id_aquirer);
        rc = utl_CSV_insertLine(CSV_TABLE_PRODUCTS_TABLE, insert);
#ifdef PARAM_DEBUG
            printf("Foi inserida a linha = %s\n", insert);
#endif
    }

    //limpa a tabela funcional_table
    remove(CSV_TABLE_FUNCIONAL_TABLE);

    //Salva tabela funcional_table
    memset(table, 0x00, sizeof(table));
    strncpy(table, &string_data[tabela5+1], tabela6 - tabela5);
    contador = strlen(table);

    for (offset = 0; offset < (strlen(table) - 1); offset+= 5 + (num_parameters * 3))
    {
        progresso = (float)offset / ((float)contador / 100);
        
        //gui_busy_start (GUI_BUSY_PARAM_FUNCIONAL+(int)progresso, FALSE, NULL);
#ifdef SHOW_PROGRESS
            system("tput cuu1; tput dl1");
            printf("SALVANDO FUNÇÕES %d%%                         \n", (int)progresso);
#endif
        memset (sAux, 0x00, sizeof(sAux));
        sprintf(sAux ,"%.02s\n", &table[offset+3]);
        num_parameters = atoi(sAux);

        memset (sAux, 0x00, sizeof(sAux));
        snprintf(sAux , (num_parameters * 3) + 1,"%s\n", &table[offset+5]);

#ifdef PARAM_DEBUG
        printf("cod_funcional  = %.03s\n", &table[offset]);
        printf("num_parameters = %.02s\n", &table[offset+3]);
        printf("cod_parameter  = %s\n", sAux);
#endif
        memset(insert, 0x00, sizeof(insert));
        sprintf(insert, "%.03s;%.02s;%s", &table[offset], &table[offset+3], sAux);
        rc = utl_CSV_insertLine(CSV_TABLE_FUNCIONAL_TABLE, insert);
#ifdef PARAM_DEBUG
            printf("Foi inserida a linha = %s\n", insert);
#endif
       
    }

    //limpa a tabela parameter_table
    remove(CSV_TABLE_PARAMETER_TABLE);
    
    //Salva tabela parameter_table
    memset(table, 0x00, sizeof(table));
    strncpy(table, &string_data[tabela6+1], tabela7 - tabela6);
    contador = strlen(table);

    for (offset = 0; offset < (strlen(table) - 1); offset+=81)
    {
        progresso = (float)offset / ((float)contador / 100);
        
        //gui_busy_start (GUI_BUSY_PARAM_PARAMETER+(int)progresso, FALSE, NULL);
#ifdef SHOW_PROGRESS
            system("tput cuu1; tput dl1");
            printf("SALVANDO PARAMETROS %d%%                         \n", (int)progresso);
#endif
#ifdef PARAM_DEBUG
            printf("cod_parameter       = %.03s\n" , &table[offset]);
            printf("cod_function        = %.02s\n" , &table[offset+3]);
            printf("transaction_label   = %.20s\n" , &table[offset+5]);
            printf("byte_1              = %.02s\n" , &table[offset+25]);
            printf("byte_2              = %.02s\n" , &table[offset+27]);
            printf("val_min_transaction = %.012s\n", &table[offset+29]);
            printf("val_max_transaction = %.012s\n", &table[offset+41]);
            printf("min_installments    = %.02s\n" , &table[offset+53]);
            printf("max_installments    = %.02s\n" , &table[offset+55]);
            printf("val_min_installment = %.012s\n", &table[offset+57]);
            printf("val_max_installment = %.012s\n", &table[offset+69]);
#endif

        //Da o insert na tabela parameter_table
            memset(insert, 0x00, sizeof(insert));
            sprintf(insert, "%.03s;%.02s;%.20s;%.02s;%.02s;%.12s;%.12s;%.02s;%.02s;%.12s;%.12s", 
                &table[offset]   , &table[offset+3] , &table[offset+5] , &table[offset+25], &table[offset+27], &table[offset+29], 
                &table[offset+41], &table[offset+53], &table[offset+55], &table[offset+57], &table[offset+69]);
            rc = utl_CSV_insertLine(CSV_TABLE_PARAMETER_TABLE, insert);
#ifdef PARAM_DEBUG
            printf("Foi inserida a linha = %s\n", insert);
#endif
    }


    //limpa a tabela parameter_table
    remove(CSV_TABLE_CONTACTLESS_TABLE);
    
    //Salva tabela parameter_table
    memset(table, 0x00, sizeof(table));
    strncpy(table, &string_data[tabela7+1], tamanho - tabela7);
    contador = strlen(table);

    for (offset = 0; offset < (strlen(table) - 1); offset+=121)
    {
        progresso = (float)offset / ((float)contador / 100);
        
        //gui_busy_start (GUI_BUSY_PARAM_PARAMETER+(int)progresso, FALSE, NULL);
#ifdef SHOW_PROGRESS
            system("tput cuu1; tput dl1");
            printf("SALVANDO DADOS CTLS %d%%                         \n", (int)progresso);
#endif
#ifdef PARAM_DEBUG
            printf("cod_AID             = %.02s\n" , &table[offset]);
            printf("capabilities        = %.06s\n" , &table[offset+2]);
            printf("adc_capabilities    = %.010s\n", &table[offset+8]);
            printf("limit_trans         = %.08s\n" , &table[offset+18]);
            printf("limit_CDCVM         = %.08s\n" , &table[offset+26]);
            printf("limit_offline       = %.08s\n" , &table[offset+34]);
            printf("limit_NOCVM         = %.08s\n" , &table[offset+42]);
            printf("TAC_default         = %.010s\n", &table[offset+50]);
            printf("TAC_denial          = %.010s\n", &table[offset+60]);
            printf("TAC_online          = %.010s\n", &table[offset+70]);
            printf("solution_type       = %.01s\n" , &table[offset+80]);
            printf("action_zeroAM       = %.01s\n" , &table[offset+81]);
            printf("magstripe_version   = %.04s\n" , &table[offset+82]);
            printf("issuer_script       = %.01s\n" , &table[offset+86]);
            printf("magstripe_mode      = %.01s\n" , &table[offset+87]);
            printf("mobile_verify       = %.01s\n" , &table[offset+88]);
            printf("reserved_field      = %.032s\n", &table[offset+89]);
#endif

        //Da o insert na tabela parameter_table
        memset(insert, 0x00, sizeof(insert));
            sprintf(insert, "%.02s;%.06s;%.010s;%.08s;%.08s;%.08s;%.08s;%.010s;%.010s;%.010s;%.01s;%.01s;%.04s;%.01s;%.01s;%.01s;%.032s", 
                &table[offset]   , &table[offset+2] , &table[offset+8] , &table[offset+18], &table[offset+26], &table[offset+34], &table[offset+42], 
                &table[offset+50], &table[offset+60], &table[offset+70], &table[offset+80], &table[offset+81], &table[offset+82], &table[offset+86], 
                &table[offset+87], &table[offset+88], &table[offset+89]);
            rc = utl_CSV_insertLine(CSV_TABLE_CONTACTLESS_TABLE, insert);
#ifdef PARAM_DEBUG
            printf("Foi inserida a linha = %s\n", insert);
#endif
    }

    //limpa a tabela products_funcional
    remove(CSV_TABLE_PRODUCT_PARAMETERS);

    //Selecionar todos os FUNCIONAL_TABLE_COD_PRODUCT e os armazenar em uma string, um por linha
    
    //Relaciona a tabela de produtos com a tabela de funcionalidades em products_funcional
    memset(array, 0x00, sizeof(array));
    utl_CSV_GetColumn(CSV_TABLE_FUNCIONAL_TABLE, FUNCIONAL_TABLE_COD_PRODUCT, array, "\n");
    strcat(array, "\n");

    contador = strlen(array);

    memset(sAux, 0x00, sizeof(sAux));
    last_pos = 0;
    
    for (offset = 0; offset < strlen(array) + 1 - 1 ; offset++)
    {
        progresso = (float)offset / ((float)contador / 100);
        
#ifdef SHOW_PROGRESS
            system("tput cuu1; tput dl1");
            printf("ATRELANDO FUNÇÕES %d%%                         \n", (int)progresso);
#endif
        if(array[offset] == '\n') // quando quebrar uma linha
        {
            snprintf(sAux, (offset + 1) - last_pos, "%s", &array[last_pos]);
            sprintf(product, "%03d", atoi(sAux));
            last_pos = offset + 1;

            memset (where, 0x00, sizeof(where));
            sprintf(where, "cod_funcional = %d", atoi(product));

            memset(array_cod, 0x00, sizeof(array_cod));
            utl_CSV_GetRegisterByID(CSV_TABLE_FUNCIONAL_TABLE, 
                                    FUNCIONAL_TABLE_COD_PRODUCT, 
                                    FUNCIONAL_TABLE_COD_PARAMETER, 
                                    atoi(product), 
                                    array_cod);
           
            memset(nro_cod, 0x00, sizeof(nro_cod));
            utl_CSV_GetRegisterByID(CSV_TABLE_FUNCIONAL_TABLE, 
                                    FUNCIONAL_TABLE_COD_PRODUCT, 
                                    FUNCIONAL_TABLE_NUM_PARAMETERS, 
                                    atoi(product), 
                                    nro_cod);

            for (ptrArray = 0; ptrArray < (strlen(array_cod) - 1); ptrArray+=3)
            {

#ifdef PARAM_DEBUG
                printf("cod_function    = %.03s\n", &array_cod[ptrArray]);
                printf("cod_parameter   = %.03s\n", product);
#endif

            //Da o insert na tabela products_funcional
            //Ta com o nome errado esta merda pq eu sou um animal
            //A coluna cod_product da tabela product_parameters na verdade é a cod_funcional da tabela funcional_table
                memset(insert, 0x00, sizeof(insert));
                sprintf(insert, "%.03s;%.03s", product, &array_cod[ptrArray]);
                rc = utl_CSV_insertLine(CSV_TABLE_PRODUCT_PARAMETERS, insert);
#ifdef PARAM_DEBUG
            printf("Foi inserida a linha = %s\n", insert);
#endif
            }

        }

    }

    fclose(PARAM_FILE);
#ifdef SHOW_PROGRESS
            //Obtem tempo final
            gettimeofday(&fim, NULL);
            system("tput cuu1; tput dl1");
            printf("CONCLUIDO EM %.2f SEGUNDOS\n", (GET_MS(inicio, fim) / 1000000.0));
#endif

    return rc;
}