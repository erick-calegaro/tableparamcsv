#ifndef H_UTL_CSV
#define H_UTL_CSV

//Procura um caractere em uma string e retorna um inteiro com a posição da primeira ocorrencia dele
//Ex.: size = utl_CSV_FindChar (line, ';');
int  utl_CSV_FindChar 		(char *str, char to_find);

//Remove quebras de linha com \r ou \n para poder anexar a uma lista
//Ex.: utl_CSV_RemoveNewLine(zRead, zBuff);
void utl_CSV_RemoveNewLine 	(char input[], char output[]);

//Transforma um caractere hexadecimal em 4 caracteres binarios
//Ex.: utl_CSV_Convert_HexaToBin(establishmentByte2Hex, establishmentByte2Bin);
void utl_CSV_Convert_HexaToBin(char h[], char *result);

//Insere uma nova linha em formato CSV no arquivo da tabela
//Ex.: utl_CSV_insertLine(CSV_TABLE_FUNCIONAL_TABLE, "001;06;001002003004009423\n");
int  utl_CSV_insertLine 	(char *filename, char *values);

//Retorna a quantidade de linhas e o conteudo de um arquivo em formato de lista
//Ex.: lines = utl_CSV_ReadToList(CSV_TABLE_FUNCIONAL_TABLE, list, 100);
int  utl_CSV_ReadToList 	(char *filename, char st[][1024], int sizeSt);

//Retorna os itens e a quantidade de registros de uma coluna com um separador pre-definido
//Ex.: lines = utl_CSV_GetColumn(CSV_TABLE_FUNCIONAL_TABLE, FUNCIONAL_TABLE_COD_PRODUCT, column, "\n");
int  utl_CSV_GetColumn		(char * filename, int column, char * result, char * separator);

//Retorna a linha e seu tamanho de acordo com o id e a coluna de pesquisa
//Ex.: tamLine = utl_CSV_GetLineByID(CSV_TABLE_FUNCIONAL_TABLE, FUNCIONAL_TABLE_COD_PRODUCT, 1, line);
int  utl_CSV_GetLineByID	(char * filename, int column, int id, char * result);

//Retorna o registro e seu tamanho de acordo com o id e a coluna de pesquisa
//Ex.: tamRegist = utl_CSV_GetRegisterByID(CSV_TABLE_FUNCIONAL_TABLE, FUNCIONAL_TABLE_COD_PRODUCT, FUNCIONAL_TABLE_COD_PARAMETER, 1, regist);
int  utl_CSV_GetRegisterByID(char * filename, int columnId, int columnReg, int id, char * result);


int utl_CSV_GetRegisterByIDs(char * filename, int columnIdA, int idA, int columnIdB, int idB, int columnReg, char * result);

//Retorna uma lista de ocorrencias e seu tamanho de acordo com o id e a coluna de pesquisa
//Ex.: lines = utl_CSV_GetListByID(CSV_TABLE_BIN_TABLE, BIN_TABLE_COD_PRODUCT, 1, list);
int utl_CSV_GetListByID 	(char * filename, int column, int id, char listOut[][1024]);

//Retorna uma lista de bins e produtos de acordo com o bin passado
//Ex.: lines = utl_CSV_GetListByBin(5502097506, list);
int utl_CSV_GetListByBin 	(long long int bin, char listOut[][1024]);

//Retorna uma lista de produtos de acordo com o tipo passado
//Ex.: lines = utl_CSV_GetListByType(1, list);
int utl_CSV_GetListByType(int type, char listOut[][1024]);

//Retorna uma lista de funcionalidades de acordo com a função passada
//Ex.: lines = utl_CSV_GetListByType(1, list);
int utl_CSV_GetListByFunction(int function, char listOut[][1024]);

//Retorna uma lista de parâmetros de transação de acordo com o tipo de função passada
//Ex.: lines = utl_CSV_GetListByType(1, list);
int utl_CSV_GetListByTypeFunction(int typeFunction, char listOut[][1024]);

//Retorna uma unica ocorrencia com os dados de bins e de produto concatenados
//Ex.: line = utl_CSV_JoinProductByBin(atoll(pan), tipoFuncao, list);
int utl_CSV_JoinProductByBin(long long int bin, int type, char * listOut);

//Retorna uma unica ocorrencia com os parâmetros do tipo da transação
// typeFuncao é PARAMETER_TABLE_COD_FUNCTION    da tabela CSV_TABLE_PARAMETER_TABLE
// function é   PRODUCT_PARAMETERS_COD_FUNCTION da tabela CSV_TABLE_PRODUCT_PARAMETERS
//Ex.: line = utl_CSV_JoinParamByFunction(typeFuncao, function, list);
int utl_CSV_JoinParamByFunction(int typeFuncao, int function, char * listOut);

#endif