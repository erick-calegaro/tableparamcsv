int utl_KAV_FindChar(char *str, char to_find)
{
  int i;

    if(str[0] == 0 && to_find == 0)
    return(0);

    for(i = 0; str[i]; i++)
  {
    if(str[i] == to_find)
      return(i);
  }

    return(0);
}

void utl_KAV_RemoveNewLine(char input[], char output[])
{
  int  len    = 0;
  int  c      = 0;
  char out[2050]  = {0x00};

     len    = strlen(input);

     while (c < len)
     {
       if(input[c] == '\r' || input[c] == '\n')
       {
         // DO NOTHING
       }
       else
       {
         out[c] = input[c];
       }

       c++;
     }

     out[c] = '\0';

   strcpy(output, out);

   return;
}

int  utl_KAV_Insert (char * filename, char * key, char * value)
{
  FILE  *pont_arq;
  char  inputList   [MAX_LINES_KAV][1024];
  char  outputList  [MAX_LINES_KAV][1024];
  char  zRead[1024] = {0x00};
  char  zBuff[1024] = {0x00};
  char  * cls;
  int   count   = 0;
  int   i       = 0;
  int   size    = 0;

  pont_arq  = fopen(filename, "r");
  count = 0;
  while(!feof(pont_arq))
  {
    memset(zRead, 0x00, sizeof(zRead));
    memset(zBuff, 0x00, sizeof(zBuff));
    cls = fgets(zRead, 1024, pont_arq);
    if(strlen(zRead) > 0)
    {
      utl_KAV_RemoveNewLine(zRead, zBuff);
      if(count < MAX_LINES_KAV){
        strcpy(outputList[count], zBuff);
      }else{
        break;
      }
      count++;
    }
    //printf("szText = %s\n", zBuff);
  }
  fclose(pont_arq);

  pont_arq = fopen(filename, ATTRIB_WRITE);
  
  if(pont_arq == NULL)
    return 1;

  for (i = 0; i < count; i++)
  {
    fprintf(pont_arq, "%s\n", outputList[i]);
    //printf("szText = %s\n", outputList[i]);
  }

  fprintf(pont_arq, "%s=%s\n", key, value);

  fclose(pont_arq);

  return(count);
}

int  utl_KAV_Update (char * filename, char * key, char * value)
{
  FILE  *pont_arq;
  char  inputList   [MAX_LINES_KAV][1024];
  char  outputList  [MAX_LINES_KAV][1024];
  char  zRead[1024] = {0x00};
  char  zBuff[1024] = {0x00};
  char  * cls;
  int   count   = 0;
  int   i       = 0;
  int   size    = 0;

  pont_arq  = fopen(filename, "r");
  count = 0;
  while(!feof(pont_arq))
  {
    memset(zRead, 0x00, sizeof(zRead));
    memset(zBuff, 0x00, sizeof(zBuff));
    cls = fgets(zRead, 1024, pont_arq);
    if(strlen(zRead) > 0)
    {
      utl_KAV_RemoveNewLine(zRead, zBuff);
      if(count < MAX_LINES_KAV){
        strcpy(inputList[count], zBuff);
      }else{
        break;
      }
      count++;
    }
  }
  fclose(pont_arq);

  for(i = 0; i < count; i++)
  {
    strcpy(zRead, inputList[i]);

    size = utl_KAV_FindChar (zRead, '=');

    if (strncmp(zRead, key, size) == 0)
    {
      sprintf(zBuff, "%s=%s", key, value);
      strcpy(outputList[i], zBuff);
    }
    else
    {
      strcpy(outputList[i], inputList[i]);
    }
  }

  pont_arq = fopen(filename, ATTRIB_WRITE);
  
  if(pont_arq == NULL)
    return 1;

  for (i = 0; i < count; i++)
  {
    fprintf(pont_arq, "%s\n", outputList[i]);
  }

  fclose(pont_arq);

  return(count);
}

int  utl_KAV_Select (char * filename, char * key, char * value)
{
    char zRead[1024] = {0x00};
    char zBuff[1024] = {0x00};
    char zAux  [64]   = {0x00};
    int  tamKey       = 0;

    sprintf(zAux, "%s=", key);
    tamKey = strlen(zAux);
    
    FILE *pont_arq = fopen(filename, ATTRIB_READ);
    if(!pont_arq)
    {
        return RC_OK;
    }

    while(!feof(pont_arq))
    {
        fgets(zRead, 1024, pont_arq);
        utl_KAV_RemoveNewLine(zRead, zBuff);
        if(!strncmp(zBuff, zAux, tamKey))
            strcpy(value, &zBuff[tamKey]);

        //printf("szText = %s\n", zBuff);
    }
    
    return RC_OK;

}

int  utl_KAV_Delete (char * filename, char * key)
{
  FILE  *pont_arq;
  char  inputList   [MAX_LINES_KAV][1024];
  char  outputList  [MAX_LINES_KAV][1024];
  char  zRead[1024] = {0x00};
  char  zBuff[1024] = {0x00};
  char  * cls;
  int   count   = 0;
  int   i       = 0;
  int   size    = 0;

  pont_arq  = fopen(filename, "r");
  count = 0;
  while(!feof(pont_arq))
  {
    memset(zRead, 0x00, sizeof(zRead));
    memset(zBuff, 0x00, sizeof(zBuff));
    cls = fgets(zRead, 1024, pont_arq);
    if(strlen(zRead) > 0)
    {
      utl_KAV_RemoveNewLine(zRead, zBuff);
      if(count < MAX_LINES_KAV){
        strcpy(inputList[count], zBuff);
      }else{
        break;
      }
      count++;
    }
  }
  fclose(pont_arq);

  pont_arq = fopen(filename, ATTRIB_WRITE);
  
  if(pont_arq == NULL)
    return 1;

  for(i = 0; i < count; i++)
  {
    strcpy(zRead, inputList[i]);

    size = utl_KAV_FindChar (zRead, '=');

    if (strncmp(zRead, key, size) != 0)
    {
      fprintf(pont_arq, "%s\n", zRead);
    }
  }

  fclose(pont_arq);

  return(count);
}
