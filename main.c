#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>

#define LOCAL_RUN
//#define PARAM_DEBUG
#define APPLY_PARAMETERS
#define SHOW_PROGRESS 
 
#include "defines.h"

#include "utl_CSV.h"
#include "utl_CSV.c"

#include "utl_KAV.h"
#include "utl_KAV.c"

#include "setParametersCSV.c"
#include "getParametersCSV.c"

#include "saveTaxes.c"

int main(int argc, char const *argv[])
{
	char value[1024] = {0x00};
	char aux[1024] = {0x00};
	char list[32][1024] = {0x00};
	int i = 0;

	// utl_KAV_Insert (KAV_TABLE_CONFIG_TABLE, CONFIG_TABLE_TESTE_INSERT, "OLA MUNDO!");
	// utl_KAV_Update (KAV_TABLE_CONFIG_TABLE, CONFIG_TABLE_TESTE_UPDATE, "OLA MUNDO!");
	// utl_KAV_Delete (KAV_TABLE_CONFIG_TABLE, CONFIG_TABLE_TESTE_DELETE);
	// utl_KAV_Select (KAV_TABLE_CONFIG_TABLE, CONFIG_TABLE_TESTE_SELECT, value);
	// printf("value = %s\n", value);
	

	//saveParameters();
	// getBinProduct ("5346950000", "01");
	// getProductParameters (1, 1);
	//fourDigitsRequiredByProduct("FF", "FF");
	//cvvRequiredByProduct("FF", "FF");
	//pwdRequiredByProduct("FF", "FF");
	//entryModeByProduct("801", "FF", "FF");
	//cvvIllegibleByProduct("00", "00");


	saveParameters();
	saveTaxes();

	utl_CSV_GetColumn(CSV_TABLE_TAX_TABLE, TAX_TABLE_PRODUCT, aux, " ");
	puts(aux);

	utl_CSV_GetRegisterByIDs(CSV_TABLE_INST_TABLE, INST_TABLE_MODALITY, 10, INST_TABLE_PRODUCT, 80, INST_TABLE_FEE, aux);
	puts(aux);

	utl_CSV_GetLineByID(CSV_TABLE_INST_TABLE, INST_TABLE_MODALITY, 9, aux);
	puts(aux);

	utl_CSV_ReadToList(CSV_TABLE_INST_TABLE, list, 11);
	for(i = 0; i < 11; i++)
		printf("[%s]\n", list[i]);

	return 0;
}