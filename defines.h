#ifndef H_DEFINES
#define H_DEFINES

#define RC_OK 								 0
#define RC_ERR 								 1

#define ATTRIB_READ   						"r"
#define ATTRIB_WRITE  						"w"
#define ATTRIB_APPEND 						"a+"

#define BOOLE								unsigned int
#define BOOL								unsigned int

#define TRUE  								1
#define FALSE 								0

#define MAX_LINES_KAV						512

#define PARAM_TABLES_PERM 					 "./res/TABPARAM.DAT"
#define TAX_TABLES_PERM 					 "./res/TABTAX.DAT"

//(fim*x+ms) - (inicio*x+ms) = tempo
#define GET_MS(ini, fim)  ((fim.tv_sec * 1000000 + fim.tv_usec) \
            - (ini.tv_sec * 1000000 + ini.tv_usec))

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//NOMES DAS TABELAS

//ARQUIVOS EM CSV (COMMA SEPARATED VALUES)
#define CSV_TABLE_ACQUIRER_TABLE             "./reg/acquirer_table.csv"
#define CSV_TABLE_EC_DATA                    "./reg/ec_data.csv"
#define CSV_TABLE_BIN_TABLE                  "./reg/bin_table.csv"
#define CSV_TABLE_PRODUCTS_TABLE             "./reg/products_table.csv"
#define CSV_TABLE_FUNCIONAL_TABLE            "./reg/funcional_table.csv"
#define CSV_TABLE_PARAMETER_TABLE            "./reg/parameter_table.csv"
#define CSV_TABLE_PRODUCT_PARAMETERS         "./reg/product_parameters.csv"
#define CSV_TABLE_TAX_TABLE                  "./reg/tax_table.csv"
#define CSV_TABLE_INST_TABLE                 "./reg/inst_table.csv"
#define CSV_TABLE_CONTACTLESS_TABLE          "./reg/ctls_table.csv"

//ARQUIVOS EM KAV (KEY AND VALUE)
#define KAV_TABLE_CONFIG_TABLE     	     	 "./reg/config.kav"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//NOMES DAS COLUNAS

//TABELA DE ADIQUIRENTES
#define ACQUIRER_TABLE_COL_ID_ACQUIRER 		0
#define ACQUIRER_TABLE_COL_ACQUIRER_CODE 	1
#define ACQUIRER_TABLE_COL_NAME 			2

//TABELA DE ESTABELECIMENTOS
#define EC_DATA_NAME 						0
#define EC_DATA_ADDRESS_1					1
#define EC_DATA_ADDRESS_2					2
#define EC_DATA_SIMBOL						3
#define EC_DATA_CURRENCY_CODE				4
#define EC_DATA_CURRENCY_EXPONENT			5
#define EC_DATA_COUNTRY_CODE				6
#define EC_DATA_CATEGORY_CODE				7
#define EC_DATA_BYTE_1						8
#define EC_DATA_BYTE_2						9
#define EC_DATA_BYTE_3						10
#define EC_DATA_ID_ACQUIRER					11

//TABELA DE PRODUTOS POR RANGE DE BINS
#define BIN_TABLE_ID_ACQUIRER 				0
#define BIN_TABLE_BIN_INITIAL				1
#define BIN_TABLE_BIN_FINAL					2
#define BIN_TABLE_COD_PRODUCT				3
#define BIN_TABLE_BYTE_1					4
#define BIN_TABLE_BYTE_2					5
#define BIN_TABLE_BYTE_3					6

//TABELA DE PARAMETROS DOS PRODUTOS
#define PRODUCTS_TABLE_COD_PRODUCT			0
#define PRODUCTS_TABLE_NAME_PRODUCT			1
#define PRODUCTS_TABLE_TYPE_PRODUCT			2
#define PRODUCTS_TABLE_BYTE_1				3
#define PRODUCTS_TABLE_BYTE_2				4
#define PRODUCTS_TABLE_BYTE_3				5
#define PRODUCTS_TABLE_PAN_MASK_INITIAL		6
#define PRODUCTS_TABLE_PAN_MASK_FINAL		7
#define PRODUCTS_TABLE_COD_FUNCIONAL		8
#define PRODUCTS_TABLE_COD_AID				9
#define PRODUCTS_TABLE_ID_ACQUIRER			10

//TABELA DE FUNCIONALIDADES POR PRODUTO - BRUTA
#define FUNCIONAL_TABLE_COD_PRODUCT			0
#define FUNCIONAL_TABLE_NUM_PARAMETERS		1
#define FUNCIONAL_TABLE_COD_PARAMETER		2

//TABELA DE PARAMETROS POR FUNCIONALIDADE
#define PARAMETER_TABLE_COD_PARAMETER		0
#define PARAMETER_TABLE_COD_FUNCTION		1
#define PARAMETER_TABLE_TRANSACTION_LABEL	2
#define PARAMETER_TABLE_BYTE_1				3
#define PARAMETER_TABLE_BYTE_2				4
#define PARAMETER_TABLE_VAL_MIN_TRANSACTION	5
#define PARAMETER_TABLE_VAL_MAX_TRANSACTION	6
#define PARAMETER_TABLE_MIN_INSTALLMENTS	7
#define PARAMETER_TABLE_MAX_INSTALLMENTS	8
#define PARAMETER_TABLE_VAL_MIN_INSTALLMENT	9
#define PARAMETER_TABLE_VAL_MAX_INSTALLMENT	10

//TABELA DE PARAMETROS CONTACTLESS
#define CONTACTLESS_TABLE_COD_AID			0
#define CONTACTLESS_TABLE_CAPABILITIES		1
#define CONTACTLESS_TABLE_ADC_CAPABILITIES	2
#define CONTACTLESS_TABLE_LIMIT_TRANS		3
#define CONTACTLESS_TABLE_LIMIT_CDCVM		4
#define CONTACTLESS_TABLE_LIMIT_OFFLINE		5
#define CONTACTLESS_TABLE_LIMIT_NOCVM		6
#define CONTACTLESS_TABLE_TAC_DEFAULT		7
#define CONTACTLESS_TABLE_TAC_DENIAL		8
#define CONTACTLESS_TABLE_TAC_ONLINE		9
#define CONTACTLESS_TABLE_SOLUTION_TYPE		10
#define CONTACTLESS_TABLE_ACTION_ZEROAM		11
#define CONTACTLESS_TABLE_MAGSTRIPE_VERSION	12
#define CONTACTLESS_TABLE_ISSUER_SCRIPT		13
#define CONTACTLESS_TABLE_MAGSTRIPE_MODE	14
#define CONTACTLESS_TABLE_MOBILE_VERIFY		15
#define CONTACTLESS_TABLE_RESERVED_FIELD	16

//TABELA DE PARAMETROS POR PRODUTO
#define PRODUCT_PARAMETERS_COD_FUNCTION		0
#define PRODUCT_PARAMETERS_COD_PARAMETER	1

//RETORNOS DO JOIN ENTRE BINS E PRODUTOS
#define GETBINPRODUCT_BINS_ID_ACQUIRER		 0
#define GETBINPRODUCT_BINS_BIN_INITIAL		 1
#define GETBINPRODUCT_BINS_BIN_FINAL		 2
#define GETBINPRODUCT_BINS_COD_PRODUCT		 3
#define GETBINPRODUCT_BINS_BYTE_1			 4
#define GETBINPRODUCT_BINS_BYTE_2			 5
#define GETBINPRODUCT_BINS_BYTE_3			 6
#define GETBINPRODUCT_PARAM_COD_PRODUCT		 7
#define GETBINPRODUCT_PARAM_NAME_PRODUCT	 8
#define GETBINPRODUCT_PARAM_TYPE_PRODUCT	 9
#define GETBINPRODUCT_PARAM_BYTE_1			 10
#define GETBINPRODUCT_PARAM_BYTE_2			 11
#define GETBINPRODUCT_PARAM_BYTE_3			 12
#define GETBINPRODUCT_PARAM_PAN_MASK_INITIAL 13
#define GETBINPRODUCT_PARAM_PAN_MASK_FINAL	 14
#define GETBINPRODUCT_PARAM_COD_FUNCIONAL	 15
#define GETBINPRODUCT_PARAM_COD_AID			 16
#define GETBINPRODUCT_PARAM_ID_ACQUIRER		 17

//RETORNOS DO JOIN ENTRE PARAMETROS E PRODUTOSxPARAMETROS
#define GETPRODPARAM_FUNC_COD_FUNCTION	 	 0
#define GETPRODPARAM_FUNC_COD_PARAMETER	 	 1
#define GETPRODPARAM_PARAM_COD_PARAMETER 	 2
#define GETPRODPARAM_PARAM_COD_FUNCTION	 	 3
#define GETPRODPARAM_PARAM_TRANS_LABEL		 4
#define GETPRODPARAM_PARAM_BYTE_1	 		 5
#define GETPRODPARAM_PARAM_BYTE_2	 		 6
#define GETPRODPARAM_PARAM_VAL_MIN_TRANS	 7
#define GETPRODPARAM_PARAM_VAL_MAX_TRANS	 8
#define GETPRODPARAM_PARAM_MIN_INSTALLMENTS	 9
#define GETPRODPARAM_PARAM_MAX_INSTALLMENTS	 10
#define GETPRODPARAM_PARAM_VAL_MIN_INSTAL	 11
#define GETPRODPARAM_PARAM_VAL_MAX_INSTAL	 12

#define TAX_TABLE_INSTALLMENT				0
#define TAX_TABLE_FEE						1
#define TAX_TABLE_PRODUCT					2

#define INST_TABLE_MODALITY					0
#define INST_TABLE_FEE						1
#define INST_TABLE_PRODUCT					2


#define TAX_TABLE


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//NOMES DAS CHAVES

//TABELA DE CONFIGURAÇÕES
#define CONFIG_TABLE_TESTE_SELECT	 		 "testeSelect"
#define CONFIG_TABLE_TESTE_INSERT	 	 	 "testeInsert"
#define CONFIG_TABLE_TESTE_UPDATE	 	 	 "testeUpdate"
#define CONFIG_TABLE_TESTE_DELETE	 		 "testeDelete"

#endif