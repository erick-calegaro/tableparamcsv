
int getBinProduct (char * pan, char * modality)
{

#ifndef APPLY_PARAMETERS
    return RC_OK;
#endif

    char sAux       	  [128 + 1] = {0x00};
    char sProduct   	  [3 + 1]   = {0x00};
    char sFunction   	  [3 + 1]   = {0x00};
    char name_product 	  [32 + 1]  = {0x00};
    char prodByte_1       [2 + 1]   = {0x00};
    char prodByte_2       [2 + 1]   = {0x00};
    char prodByte_3       [2 + 1]   = {0x00};
    char binsByte_1       [2 + 1]   = {0x00};
    char binsByte_2       [2 + 1]   = {0x00};
    char binsByte_3       [2 + 1]   = {0x00};
    char selectedBIN	  [10]      = {0x00};
    uint offset     	  = 0;
    int  iProduct   	  = 0;
    int  iFunction  	  = 0;
    int  last_pos   	  = 0;
    int  pan_mask_initial = 0;
    int  pan_mask_final   = 0;
    int  dataType     	  = 0;
    int  rc         	  = RC_OK;
    int  tipoFuncao  	  = (atoi(modality) >= 50) ? 2 : 1;
    char list[1024]  	  = {0x00};
	int  line	 	 	  = 0;

    memset(selectedBIN, 0x00, sizeof(selectedBIN));
    memcpy (selectedBIN, pan, 10);

	line = utl_CSV_JoinProductByBin(atoll(pan), tipoFuncao, list) + 1;
    strncat(list, ";", 1);
	// printf("line = %d\n", line);
	// printf("%s\n", list);

    last_pos = 0;
    for (offset = 0; offset < line; offset++)
    {
        if(list[offset] == ';')
        {
            //printf("Offset = %d\n", offset);
            switch(dataType)
            {
				case GETBINPRODUCT_BINS_COD_PRODUCT:
                	memset (sAux, 0x00, sizeof(sAux));
				    sprintf(sAux, "%.03s", &list[last_pos+1]);
				    strncpy(sProduct, sAux, 3);
                break;
				case GETBINPRODUCT_BINS_BYTE_1:
                	memset (sAux, 0x00, sizeof(sAux));
				    sprintf(sAux, "%.02s", &list[last_pos+1]);
				    strncpy(binsByte_1, sAux, 2);
                break;
				case GETBINPRODUCT_BINS_BYTE_2:
                	memset (sAux, 0x00, sizeof(sAux));
				    sprintf(sAux, "%.02s", &list[last_pos+1]);
				    strncpy(binsByte_2, sAux, 2);
                break;
				case GETBINPRODUCT_BINS_BYTE_3:
                	memset (sAux, 0x00, sizeof(sAux));
				    sprintf(sAux, "%.02s", &list[last_pos+1]);
				    strncpy(binsByte_3, sAux, 2);
                break;
				case GETBINPRODUCT_PARAM_NAME_PRODUCT:
                	memset (sAux, 0x00, sizeof(sAux));
				    sprintf(sAux, "%.20s", &list[last_pos+1]);
				    strcpy(name_product, sAux);
                break;
				case GETBINPRODUCT_PARAM_BYTE_1:
                	memset (sAux, 0x00, sizeof(sAux));
				    sprintf(sAux, "%.02s", &list[last_pos+1]);
				    strncpy(prodByte_1, sAux, 2);
                break;
				case GETBINPRODUCT_PARAM_BYTE_2:
                	memset (sAux, 0x00, sizeof(sAux));
				    sprintf(sAux, "%.02s", &list[last_pos+1]);
				    strncpy(prodByte_2, sAux, 2);
                break;
				case GETBINPRODUCT_PARAM_BYTE_3:
    				strncpy(prodByte_3, "00", 2);
                break;
				case GETBINPRODUCT_PARAM_PAN_MASK_INITIAL:
                	memset(sAux, 0x00, sizeof(sAux));
				    strncpy(sAux, &list[last_pos+1], (offset+1) - last_pos);
				    pan_mask_initial = atoi(sAux);
                break;
				case GETBINPRODUCT_PARAM_PAN_MASK_FINAL:
                	memset(sAux, 0x00, sizeof(sAux));
				    strncpy(sAux, &list[last_pos+1], (offset+1) - last_pos);
				    pan_mask_final = atoi(sAux);
                break;
				case GETBINPRODUCT_PARAM_COD_FUNCIONAL:
                	memset (sAux, 0x00, sizeof(sAux));
				    sprintf(sAux, "%.03s", &list[last_pos+1]);
				    strncpy(sFunction, sAux, 3);
                break;
                default: break;
            }
            dataType++;
            last_pos = offset;
            // printf("&list[last_pos+1] = %s\n", &list[last_pos+1]);
            // printf("sAux = %s\n", sAux);
        }

    }

    iProduct  = atoi(sProduct);
    iFunction = atoi(sFunction);

#ifdef PARAM_DEBUG
    printf("iProduct          = %d\n"   , iProduct);
    printf("iFunction         = %d\n"   , iFunction);
    printf("==========================\n");
    printf("         Products         \n");
    printf("name_product      = %s\n"   , name_product);
    printf("pan_mask_initial  = %d\n"   , pan_mask_initial);
    printf("pan_mask_final    = %d\n"   , pan_mask_final);
    printf("byte_1            = %s\n"   , prodByte_1);
    printf("byte_2            = %s\n"   , prodByte_2);
    printf("byte_3            = %s\n"   , prodByte_3);
    printf("==========================\n");
    printf("           Bins           \n");
    printf("byte_1            = %s\n"   , binsByte_1);
    printf("byte_2            = %s\n"   , binsByte_2);
    printf("byte_3            = %s\n"   , binsByte_3);
    printf("==========================\n");
#endif

    return rc;
}


int getProductParameters (int typeFuncao, int function)
{

#ifndef APPLY_PARAMETERS
    return RC_OK;
#endif

    char sAux       	  [128 + 1] = {0x00};
    char transaction_label[32 + 1]  = {0x00};
    long long int val_min_transaction     	= 0;
    long long int val_max_transaction    	= 0;
    long long int val_min_installment     	= 0;
    long long int val_max_installment     	= 0;
    int min_installments     	= 0;
    int max_installments     	= 0;
    uint offset     	      	 	= 0;
    int  last_pos   	  			= 0;
    int  dataType     	  			= 0;
    int  rc         	  			= RC_OK;
    char list[1024]  	  			= {0x00};
	int  line	 	 	  			= 0;

	line = utl_CSV_JoinParamByFunction(typeFuncao, function, list) + 1;
    strncat(list, ";", 1);
	//printf("line = %d\n", line);
	//printf("%s\n", list);

    last_pos = 0;
    for (offset = 0; offset < line; offset++)
    {
        if(list[offset] == ';')
        {
            //printf("Offset = %d\n", offset);
            switch(dataType)
            {
            	case GETPRODPARAM_FUNC_COD_FUNCTION:

            	break;
				case GETPRODPARAM_FUNC_COD_PARAMETER:

				break;
				case GETPRODPARAM_PARAM_COD_PARAMETER:

				break;
				case GETPRODPARAM_PARAM_COD_FUNCTION:

				break;
				case GETPRODPARAM_PARAM_TRANS_LABEL:
                    memset (sAux, 0x00, sizeof(sAux));
				    sprintf(sAux, "%.20s", &list[last_pos+1]);
				    strcpy(transaction_label, sAux);
				break;
				case GETPRODPARAM_PARAM_BYTE_1:

				break;
				case GETPRODPARAM_PARAM_BYTE_2:

				break;
				case GETPRODPARAM_PARAM_VAL_MIN_TRANS:
					memset(sAux, 0x00, sizeof(sAux));
                    strncpy(sAux, &list[last_pos+1], (offset-1) - last_pos);
                    val_min_transaction = atoll(sAux);
				break;
				case GETPRODPARAM_PARAM_VAL_MAX_TRANS:
					memset(sAux, 0x00, sizeof(sAux));
                    strncpy(sAux, &list[last_pos+1], (offset-1) - last_pos);
                    val_max_transaction = atoll(sAux);
				break;
				case GETPRODPARAM_PARAM_MIN_INSTALLMENTS:
					memset(sAux, 0x00, sizeof(sAux));
                    strncpy(sAux, &list[last_pos+1], (offset-1) - last_pos);
                    min_installments = atoi(sAux);
				break;
				case GETPRODPARAM_PARAM_MAX_INSTALLMENTS:
					memset(sAux, 0x00, sizeof(sAux));
                    strncpy(sAux, &list[last_pos+1], (offset-1) - last_pos);
                    max_installments = atoi(sAux);
				break;
				case GETPRODPARAM_PARAM_VAL_MIN_INSTAL:
					memset(sAux, 0x00, sizeof(sAux));
                    strncpy(sAux, &list[last_pos+1], (offset-1) - last_pos);
                    val_min_installment = atoll(sAux);
				break;
				case GETPRODPARAM_PARAM_VAL_MAX_INSTAL:
					memset(sAux, 0x00, sizeof(sAux));
                    strncpy(sAux, &list[last_pos+1], (offset-1) - last_pos);
                    val_max_installment = atoll(sAux);
				break;
                default: break;
            }
            dataType++;
            last_pos = offset;
            // printf("&list[last_pos+1] = %s\n", &list[last_pos+1]);
            // printf("sAux = %s\n", sAux);
        }

    }

#ifdef PARAM_DEBUG
    printf("========================================\n");
    printf("transaction_label   = %s\n"       , transaction_label);
    printf("val_min_transaction = %.012lld\n" , val_min_transaction);
    printf("val_max_transaction = %.012lld\n" , val_max_transaction);
    printf("min_installments    = %.02d\n"    , min_installments);
    printf("max_installments    = %.02d\n"    , max_installments);
    printf("val_min_installment = %.012lld\n" , val_min_installment);
    printf("val_max_installment = %.012lld\n" , val_max_installment);
    printf("========================================\n");
#endif

    return rc;
}

BOOL fourDigitsRequiredByProduct(char * productByte2Hex, char * binByte2Hex)
{

#ifndef APPLY_PARAMETERS
    return FALSE;
#endif

    char establishmentByte2Hex[2+1];
    char establishmentByte2Bin[8+1];
    char productByte2Bin[8+1];
    char binByte2Bin[8+1];
    int  rc = FALSE;

    memset(establishmentByte2Hex, 0x00, sizeof(establishmentByte2Hex));
    memset(establishmentByte2Bin, 0x00, sizeof(establishmentByte2Bin));
    memset(productByte2Bin, 0x00, sizeof(productByte2Bin));
    memset(binByte2Bin, 0x00, sizeof(binByte2Bin));

	utl_CSV_GetRegisterByID(CSV_TABLE_EC_DATA, EC_DATA_ID_ACQUIRER, EC_DATA_BYTE_2, 1, establishmentByte2Hex);

    utl_CSV_Convert_HexaToBin(establishmentByte2Hex, establishmentByte2Bin);
    utl_CSV_Convert_HexaToBin(productByte2Hex, productByte2Bin);
    utl_CSV_Convert_HexaToBin(binByte2Hex, binByte2Bin);

    if((establishmentByte2Bin[1] == '1') && (productByte2Bin[2] == '1') && (binByte2Bin[2] == '1'))
        rc = TRUE;
    else
        rc = FALSE;

#ifdef PARAM_DEBUG
    printf("establishmentByte2Hex        = %s\n", establishmentByte2Hex);
    printf("productByte2Hex              = %s\n", productByte2Hex);
    printf("binByte2Hex                  = %s\n", binByte2Hex);
    printf("establishmentByte2Bin        = %s\n", establishmentByte2Bin);
    printf("productByte2Bin              = %s\n", productByte2Bin);
    printf("binByte2Bin                  = %s\n", binByte2Bin);
    printf("fourDigitsRequiredByProduct  = %s\n", (rc) ? "SIM" : "NÃO");
#endif

    return rc;
}

BOOL cvvRequiredByProduct(char * productByte1Hex, char * binByte1Hex)
{

#ifndef APPLY_PARAMETERS
    return TRUE;
#endif


    char establishmentByte1Hex[2+1];
    char establishmentByte1Bin[8+1];
    char productByte1Bin[8+1];
    char binByte1Bin[8+1];
    int  rc = FALSE;

    memset(establishmentByte1Hex, 0x00, sizeof(establishmentByte1Hex));
    memset(establishmentByte1Bin, 0x00, sizeof(establishmentByte1Bin));
    memset(productByte1Bin, 0x00, sizeof(productByte1Bin));
    memset(binByte1Bin, 0x00, sizeof(binByte1Bin));

	utl_CSV_GetRegisterByID(CSV_TABLE_EC_DATA, EC_DATA_ID_ACQUIRER, EC_DATA_BYTE_1, 1, establishmentByte1Hex);

    utl_CSV_Convert_HexaToBin(establishmentByte1Hex, establishmentByte1Bin);
    utl_CSV_Convert_HexaToBin(productByte1Hex, productByte1Bin);
    utl_CSV_Convert_HexaToBin(binByte1Hex, binByte1Bin);


    if((establishmentByte1Bin[5] == '1') && (productByte1Bin[5] == '1') && (binByte1Bin[5] == '1'))
        rc = TRUE;
    else
        rc = FALSE;

#ifdef PARAM_DEBUG
    printf("establishmentByte1Hex = %s\n", establishmentByte1Hex);
    printf("productByte1Hex       = %s\n", productByte1Hex);
    printf("binByte1Hex           = %s\n", binByte1Hex);
    printf("establishmentByte1Bin = %s\n", establishmentByte1Bin);
    printf("productByte1Bin       = %s\n", productByte1Bin);
    printf("binByte1Bin           = %s\n", binByte1Bin);
    printf("cvvRequiredByProduct  = %s\n", (rc) ? "SIM" : "NÃO");
#endif

    return rc;
}

int entryModeByProduct(char * entryMode, char * productByte1Hex, char * binByte1Hex)
{

#ifndef APPLY_PARAMETERS
    return TRUE;
#endif
    
    char establishmentByte1Hex[2+1];
    char establishmentByte1Bin[8+1];
    char productByte1Bin[8+1];
    char binByte1Bin[8+1];
    int  rc = TRUE;

    memset(establishmentByte1Hex, 0x00, sizeof(establishmentByte1Hex));
    memset(establishmentByte1Bin, 0x00, sizeof(establishmentByte1Bin));
    memset(productByte1Bin, 0x00, sizeof(productByte1Bin));
    memset(binByte1Bin, 0x00, sizeof(binByte1Bin));

	utl_CSV_GetRegisterByID(CSV_TABLE_EC_DATA, EC_DATA_ID_ACQUIRER, EC_DATA_BYTE_1, 1, establishmentByte1Hex);

    utl_CSV_Convert_HexaToBin(establishmentByte1Hex, establishmentByte1Bin);
    utl_CSV_Convert_HexaToBin(productByte1Hex, productByte1Bin);
    utl_CSV_Convert_HexaToBin(binByte1Hex, binByte1Bin);

    //fallback
    if (memcmp(entryMode, "801", 3) == 0)
    {
        if((establishmentByte1Bin[2] == '1') && (productByte1Bin[2] == '1') && (binByte1Bin[2] == '1'))
            rc = TRUE;
        else
            rc = FALSE;
    }

    //CHIP
    if (memcmp(entryMode, "059", 3) == 0 || memcmp(entryMode, "051", 3) == 0)
    {
        if((establishmentByte1Bin[1] == '1') && (productByte1Bin[1] == '1') && (binByte1Bin[1] == '1'))
            rc = TRUE;
        else
            rc = FALSE;
    }

#ifdef PARAM_DEBUG
    printf("establishmentByte1Hex = %s\n", establishmentByte1Hex);
    printf("productByte1Hex       = %s\n", productByte1Hex);
    printf("binByte1Hex           = %s\n", binByte1Hex);
    printf("establishmentByte1Bin = %s\n", establishmentByte1Bin);
    printf("productByte1Bin       = %s\n", productByte1Bin);
    printf("binByte1Bin           = %s\n", binByte1Bin);
    printf("entryMode             = %.03s\n", entryMode);
    printf("entryModeByProduct    = %s\n", (rc) ? "PERMITIDO" : "NÃO PERMITIDO");
#endif

    return rc;
}

BOOL cvvIllegibleByProduct(char * productByte1Hex, char * binByte1Hex)
{

#ifndef APPLY_PARAMETERS
    return TRUE;
#endif

    char establishmentByte1Hex[2+1];
    char establishmentByte1Bin[8+1];
    char productByte1Bin[8+1];
    char binByte1Bin[8+1];
    int  rc = FALSE;

    memset(establishmentByte1Hex, 0x00, sizeof(establishmentByte1Hex));
    memset(establishmentByte1Bin, 0x00, sizeof(establishmentByte1Bin));
    memset(productByte1Bin, 0x00, sizeof(productByte1Bin));
    memset(binByte1Bin, 0x00, sizeof(binByte1Bin));

	utl_CSV_GetRegisterByID(CSV_TABLE_EC_DATA, EC_DATA_ID_ACQUIRER, EC_DATA_BYTE_1, 1, establishmentByte1Hex);

    utl_CSV_Convert_HexaToBin(establishmentByte1Hex, establishmentByte1Bin);
    utl_CSV_Convert_HexaToBin(productByte1Hex, productByte1Bin);
    utl_CSV_Convert_HexaToBin(binByte1Hex, binByte1Bin);

    if((establishmentByte1Bin[6] == '1') && (productByte1Bin[6] == '1') && (binByte1Bin[6] == '1'))
        rc = TRUE;
    else
        rc = FALSE;

#ifdef PARAM_DEBUG
    printf("establishmentByte1Hex = %s\n", establishmentByte1Hex);
    printf("productByte1Hex       = %s\n", productByte1Hex);
    printf("binByte1Hex           = %s\n", binByte1Hex);
    printf("establishmentByte1Bin = %s\n", establishmentByte1Bin);
    printf("productByte1Bin       = %s\n", productByte1Bin);
    printf("binByte1Bin           = %s\n", binByte1Bin);
    printf("cvvIllegibleByProduct = %s\n", (rc) ? "SIM" : "NÃO");
#endif

    return rc;
}