#ifdef  H_UTL_KAV
#define H_UTL_KAV

//Procura um caractere em uma string e retorna um inteiro com a posição da primeira ocorrencia dele
//Ex.: size = utl_KAV_FindChar (line, '=');
int utl_KAV_FindChar(char *str, char to_find);

//Remove quebras de linha com \r ou \n para poder anexar a uma lista
//Ex.: utl_KAV_RemoveNewLine(zRead, zBuff);
void utl_KAV_RemoveNewLine(char input[], char output[]);

//Insere uma nova linha de registro no arquivo de chave e valor
//Ex.: size = utl_CSV_FindChar (line, ';');
int  utl_KAV_Insert (char * filename, char * key, char * value);

//Atualiza o valor de um registro no arquivo de chave e valor
//Ex.: size = utl_CSV_FindChar (line, ';');
int  utl_KAV_Update (char * filename, char * key, char * value);

//Busca um registro no arquivo de chave e valor
//Ex.: size = utl_CSV_FindChar (line, ';');
int  utl_KAV_Select (char * filename, char * key, char * value);

//Apaga um registro no arquivo de chave e valor
//Ex.: size = utl_CSV_FindChar (line, ';');
int  utl_KAV_Delete (char * filename, char * key);

#endif