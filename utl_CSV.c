
#include "utl_CSV.h"

int utl_CSV_FindChar(char *str, char to_find)
{
	int i;

    if(str[0] == 0 && to_find == 0)
		return(0);

    for(i = 0; str[i]; i++)
	{
		if(str[i] == to_find)
			return(i);
	}

    return(0);
}

void utl_CSV_RemoveNewLine(char input[], char output[])
{
	int  len		=	0;
	int  c			=	0;
	char out[2050]	=	{0x00};

		 len		=	strlen(input);

 	 	 while (c < len)
 	 	 {
 	 		 if(input[c] == '\r' || input[c] == '\n')
 	 		 {
 	 			 //	DO NOTHING
 	 		 }
 	 		 else
 	 		 {
 	 			 out[c] = input[c];
 	 		 }

 	 		 c++;
 	 	 }

 	 	 out[c] = '\0';

   strcpy(output, out);

   return;
}

void utl_CSV_Convert_HexaToBin(char h[], char *result)
{
    int  tamanho  = strlen(h);
    int  id       = 0;
    int  i        = 0;    for (i = 0; i < tamanho; i++)
    {
        if (h[i] == '0')  {result[id]   = '0'; result[id+1] = '0'; result[id+2] = '0'; result[id+3] = '0'; id += 4;}
        if (h[i] == '1')  {result[id]   = '0'; result[id+1] = '0'; result[id+2] = '0'; result[id+3] = '1'; id += 4;}
        if (h[i] == '2')  {result[id]   = '0'; result[id+1] = '0'; result[id+2] = '1'; result[id+3] = '0'; id += 4;}
        if (h[i] == '3')  {result[id]   = '0'; result[id+1] = '0'; result[id+2] = '1'; result[id+3] = '1'; id += 4;}
        if (h[i] == '4')  {result[id]   = '0'; result[id+1] = '1'; result[id+2] = '0'; result[id+3] = '0'; id += 4;}
        if (h[i] == '5')  {result[id]   = '0'; result[id+1] = '1'; result[id+2] = '0'; result[id+3] = '1'; id += 4;}
        if (h[i] == '6')  {result[id]   = '0'; result[id+1] = '1'; result[id+2] = '1'; result[id+3] = '0'; id += 4;}
        if (h[i] == '7')  {result[id]   = '0'; result[id+1] = '1'; result[id+2] = '1'; result[id+3] = '1'; id += 4;}
        if (h[i] == '8')  {result[id]   = '1'; result[id+1] = '0'; result[id+2] = '0'; result[id+3] = '0'; id += 4;}
        if (h[i] == '9')  {result[id]   = '1'; result[id+1] = '0'; result[id+2] = '0'; result[id+3] = '1'; id += 4;}
        if (h[i] == 'a')  {result[id]   = '1'; result[id+1] = '0'; result[id+2] = '1'; result[id+3] = '0'; id += 4;}
        if (h[i] == 'b')  {result[id]   = '1'; result[id+1] = '0'; result[id+2] = '1'; result[id+3] = '1'; id += 4;}
        if (h[i] == 'c')  {result[id]   = '1'; result[id+1] = '1'; result[id+2] = '0'; result[id+3] = '0'; id += 4;}
        if (h[i] == 'd')  {result[id]   = '1'; result[id+1] = '1'; result[id+2] = '0'; result[id+3] = '1'; id += 4;}
        if (h[i] == 'e')  {result[id]   = '1'; result[id+1] = '1'; result[id+2] = '1'; result[id+3] = '0'; id += 4;}
        if (h[i] == 'f')  {result[id]   = '1'; result[id+1] = '1'; result[id+2] = '1'; result[id+3] = '1'; id += 4;}
        if (h[i] == 'A')  {result[id]   = '1'; result[id+1] = '0'; result[id+2] = '1'; result[id+3] = '0'; id += 4;}
        if (h[i] == 'B')  {result[id]   = '1'; result[id+1] = '0'; result[id+2] = '1'; result[id+3] = '1'; id += 4;}
        if (h[i] == 'C')  {result[id]   = '1'; result[id+1] = '1'; result[id+2] = '0'; result[id+3] = '0'; id += 4;}
        if (h[i] == 'D')  {result[id]   = '1'; result[id+1] = '1'; result[id+2] = '0'; result[id+3] = '1'; id += 4;}
        if (h[i] == 'E')  {result[id]   = '1'; result[id+1] = '1'; result[id+2] = '1'; result[id+3] = '0'; id += 4;}
        if (h[i] == 'F')  {result[id]   = '1'; result[id+1] = '1'; result[id+2] = '1'; result[id+3] = '1'; id += 4;}
    }    return;

}

int utl_CSV_insertLine(char *table, char *values)
{
	FILE *pont_arq;

	pont_arq = fopen(table, ATTRIB_APPEND);

	if(pont_arq == NULL)
	{
	  printf("Erro na abertura do arquivo!\n");
	  return 1;
	}

	fprintf(pont_arq, "%s\n", values);
	fclose(pont_arq);
	return(0);
}


int utl_CSV_ReadToList(char *fileName, char st[][1024], int sizeSt)
{
	FILE	*fd;
	char	*cls;
	int		count		= 0;
	char	zName  [50] = {0x00};
	char	zRead[1024] = {0x00};
	char	zBuff[1024] = {0x00};

	sprintf(zName, "%s", fileName);
	fd	=	fopen(zName, "r");
	count = 0;

	while(!feof(fd))
	{
		memset(zRead, 0x00, sizeof(zRead));
		memset(zBuff, 0x00, sizeof(zBuff));

		cls	=	fgets(zRead, 1024, fd);

		if(strlen(zRead) > 0)
		{
			utl_CSV_RemoveNewLine(zRead, zBuff);
			if(count < sizeSt){
				strcpy(st[count], zBuff);
			}else{
				break;
			}
			count++;
		}

	}

	fclose(fd);

	return(count);
}

int utl_CSV_GetColumn(char * filename, int column, char * result, char * separator)
{
	char list   [512][1024];
	char lineF  [1024] 	= {0x00};
	char lineP  [1024] 	= {0x00};
	char zAux   [1024] 	= {0x00};
	int  lines  		= 0;
	int  offset			= 0;
	int  size 			= 0;
	int  i 				= 0;
	int  j 				= 0;
	char firstEntry 	= 1;

	lines = utl_CSV_ReadToList(filename, list, 512);
	
	for(i = 0; i < lines; i++)
	{
		memset(lineF, 0x00, sizeof(lineF));
		strcpy(lineF, list[i]);
		strcpy(lineP, lineF);
		offset = 0;

		//printf("LENDO LINHA %d DE %d - (%ld) [%s]\n", i + 1, lines, strlen(line), line);

		for (j = 0; j <= column; j++)
		{
			size = utl_CSV_FindChar (lineP, ';');
			if (!size)
				size = strlen(lineF) - offset;

			if (j == column)
			{
				memset(zAux, 0x00, sizeof(zAux));	
				strncpy(zAux, lineP, size);
				if (firstEntry)
					firstEntry--;
				else
					strcat(result, separator);
				strcat(result, zAux);
			}else
			{
				offset += size + 1;
				strcpy(lineP, &lineF[offset]);
			}

		}
		
		//printf("LENDO COLUNA %d DA LINHA %d - (%ld) [%s]\n", column, i + 1, strlen(zAux), zAux);		
	}

	return lines;
}

int utl_CSV_GetLineByID(char * filename, int column, int id, char * result)
{
	char list   [512][1024];
	char lineF  [1024] 	= {0x00};
	char lineP  [1024] 	= {0x00};
	char zAux   [1024] 	= {0x00};
	int  lines  		= 0;
	int  offset			= 0;
	int  size 			= 0;
	int  i 				= 0;
	int  j 				= 0;
	int  idActual		= 0;

	lines = utl_CSV_ReadToList(filename, list, 512);
	
	for(i = 0; i < lines; i++)
	{
		memset(lineF, 0x00, sizeof(lineF));
		strcpy(lineF, list[i]);
		strcpy(lineP, lineF);
		offset = 0;

		//printf("LENDO LINHA %d DE %d - (%ld) [%s]\n", i + 1, lines, strlen(lineF), lineF);

		for (j = 0; j <= column; j++)
		{
			size = utl_CSV_FindChar (lineP, ';');
			if (!size)
				size = strlen(lineF) - offset;

			if (j == column)
			{
				memset(zAux, 0x00, sizeof(zAux));	
				strncpy(zAux, lineP, size);
				idActual = atoi(zAux);

				if (idActual == id)
				{
					strcpy(result, lineF);
					size = strlen(lineF);
					return size;
				}

			}else
			{
				offset += size + 1;
				strcpy(lineP, &lineF[offset]);
			}
		}
		//printf("LENDO COLUNA %d DA LINHA %d - (%ld) [%s]\n", column, i + 1, strlen(zAux), zAux);		
	}
	
	return 0;

}

int utl_CSV_GetRegisterByID(char * filename, int columnId, int columnReg, int id, char * result)
{
	char list   [512][1024];
	char lineF  [1024] 	= {0x00};
	char lineP  [1024] 	= {0x00};
	char zAux   [1024] 	= {0x00};
	int  lines  		= 0;
	int  offset			= 0;
	int  size 			= 0;
	int  i 				= 0;
	int  j 				= 0;
	int  k 				= 0;
	int  idActual		= 0;

	lines = utl_CSV_ReadToList(filename, list, 512);
	
	for(i = 0; i < lines; i++)
	{
		memset(lineF, 0x00, sizeof(lineF));
		strcpy(lineF, list[i]);
		strcpy(lineP, lineF);
		offset = 0;

		//printf("LENDO LINHA %d DE %d - (%ld) [%s]\n", i + 1, linesA, strlen(lineF), lineF);

		for (j = 0; j <= columnId; j++)
		{
			size = utl_CSV_FindChar (lineP, ';');
			if (!size)
				size = strlen(lineF) - offset;

			if (j == columnId)
			{
				memset(zAux, 0x00, sizeof(zAux));	
				strncpy(zAux, lineP, size);
				idActual = atoi(zAux);

				if (idActual == id)
				{
					strcpy(lineP, lineF);
					offset = 0;
					
					for (k = 0; k <= columnReg; k++)
					{
						size = utl_CSV_FindChar (lineP, ';');
						if (!size)
							size = strlen(lineF) - offset;

						if (k == columnReg)
						{
							memset(zAux, 0x00, sizeof(zAux));	
							strncpy(zAux, lineP, size);
							strcpy(result, zAux);
							size = strlen(result);
							return size;
						}else
						{
							offset += size + 1;
							strcpy(lineP, &lineF[offset]);
						}
					}

					return 0;
				}

			}else
			{
				offset += size + 1;
				strcpy(lineP, &lineF[offset]);
			}
		}
		//printf("LENDO COLUNA %d DA LINHA %d - (%ld) [%s]\n", columnId, i + 1, strlen(zAux), zAux);		
	}
	
	return 0;

}

int utl_CSV_GetListByID(char * filename, int column, int id, char listOut[][1024])
{
	char list   [512][1024];
	char lineF  [1024] 	= {0x00};
	char lineP  [1024] 	= {0x00};
	char zAux   [1024] 	= {0x00};
	int  lines  		= 0;
	int  offset			= 0;
	int  size 			= 0;
	int  i 				= 0;
	int  j 				= 0;
	int  k				= 0;
	int  idActual		= 0;

	lines = utl_CSV_ReadToList(filename, list, 512);
	
	for(i = 0; i < lines; i++)
	{
		memset(lineF, 0x00, sizeof(lineF));
		strcpy(lineF, list[i]);
		strcpy(lineP, lineF);
		offset = 0;

		//printf("LENDO LINHA %d DE %d - (%ld) [%s]\n", i + 1, lines, strlen(lineF), lineF);

		for (j = 0; j <= column; j++)
		{
			size = utl_CSV_FindChar (lineP, ';');
			if (!size)
				size = strlen(lineF) - offset;

			if (j == column)
			{
				memset(zAux, 0x00, sizeof(zAux));	
				strncpy(zAux, lineP, size);
				idActual = atoi(zAux);

				if (idActual == id)
				{
					strcpy(listOut[k], lineF);
					k++;
				}

			}else
			{
				offset += size + 1;
				strcpy(lineP, &lineF[offset]);
			}
		}
		//printf("LENDO COLUNA %d DA LINHA %d - (%ld) [%s]\n", column, i + 1, strlen(zAux), zAux);		
	}
	
	return k;

}

int utl_CSV_GetListByBin(long long int bin, char listOut[][1024])
{
	long long int  binInitial	= 0;
	long long int  binFinal		= 0;
	char list   [512][1024];
	char lineF  [1024] 	= {0x00};
	char lineP  [1024] 	= {0x00};
	char zAux   [1024] 	= {0x00};
	int  lines  		= 0;
	int  offset			= 0;
	int  size 			= 0;
	int  i 				= 0;
	int  j 				= 0;
	int  k				= 0;
	

	lines = utl_CSV_ReadToList(CSV_TABLE_BIN_TABLE, list, 512);
	
	for(i = 0; i < lines; i++)
	{
		memset(lineF, 0x00, sizeof(lineF));
		strcpy(lineF, list[i]);
		strcpy(lineP, lineF);
		offset = 0;

		//printf("LENDO LINHA %d DE %d - (%ld) [%s]\n", i + 1, lines, strlen(lineF), lineF);

		for (j = 0; j <= BIN_TABLE_BIN_FINAL; j++)
		{
			size = utl_CSV_FindChar (lineP, ';');
			if (!size)
				size = strlen(lineF) - offset;

			if (j == BIN_TABLE_BIN_INITIAL)
			{
				memset(zAux, 0x00, sizeof(zAux));	
				strncpy(zAux, lineP, size);
				binInitial = atoll(zAux);

			}else if (j == BIN_TABLE_BIN_FINAL)
			{
				offset += size + 1;
				strcpy(lineP, &lineF[offset]);

				memset(zAux, 0x00, sizeof(zAux));	
				strncpy(zAux, lineP, size);
				binFinal = atoll(zAux);
/*
				printf("bin        = %lld\n", bin);
				printf("binInitial = %lld\n", binInitial);
				printf("binFinal   = %lld\n", binFinal);
*/
				if ((binInitial <= bin) && (binFinal >= bin))
				{
					strcpy(listOut[k], lineF);
					k++;
				}
			}else
			{
				offset += size + 1;
				strcpy(lineP, &lineF[offset]);
			}
		}
		//printf("LENDO COLUNA %d DA LINHA %d - (%ld) [%s]\n", column, i + 1, strlen(zAux), zAux);		
	}
	
	return k;

}

int utl_CSV_GetListByType(int type, char listOut[][1024])
{
	char list   [512][1024];
	char lineF  [1024] 	= {0x00};
	char lineP  [1024] 	= {0x00};
	char zAux   [1024] 	= {0x00};
	int  lines  		= 0;
	int  offset			= 0;
	int  size 			= 0;
	int  i 				= 0;
	int  j 				= 0;
	int  k				= 0;
	int  typeActual		= 0;

	lines = utl_CSV_ReadToList(CSV_TABLE_PRODUCTS_TABLE, list, 512);
	
	for(i = 0; i < lines; i++)
	{
		memset(lineF, 0x00, sizeof(lineF));
		strcpy(lineF, list[i]);
		strcpy(lineP, lineF);
		offset = 0;

		//printf("LENDO LINHA %d DE %d - (%ld) [%s]\n", i + 1, lines, strlen(lineF), lineF);

		for (j = 0; j <= PRODUCTS_TABLE_TYPE_PRODUCT; j++)
		{
			size = utl_CSV_FindChar (lineP, ';');
			if (!size)
				size = strlen(lineF) - offset;

			if (j == PRODUCTS_TABLE_TYPE_PRODUCT)
			{
				memset(zAux, 0x00, sizeof(zAux));	
				strncpy(zAux, lineP, size);
				typeActual = atoi(zAux);
				//printf("typeActual = %d\n", typeActual);

				if (typeActual == type)
				{
					strcpy(listOut[k], lineF);
					k++;
				}
			}else
			{
				offset += size + 1;
				strcpy(lineP, &lineF[offset]);
			}
		}
		//printf("LENDO COLUNA %d DA LINHA %d - (%ld) [%s]\n", column, i + 1, strlen(zAux), zAux);		
	}
	
	return k;

}

int utl_CSV_GetListByFunction(int function, char listOut[][1024])
{
	char list   [512][1024];
	char lineF  [1024] 	= {0x00};
	char lineP  [1024] 	= {0x00};
	char zAux   [1024] 	= {0x00};
	int  lines  		= 0;
	int  offset			= 0;
	int  size 			= 0;
	int  i 				= 0;
	int  j 				= 0;
	int  k				= 0;
	int  typeActual		= 0;

	lines = utl_CSV_ReadToList(CSV_TABLE_PRODUCT_PARAMETERS, list, 512);
	
	for(i = 0; i < lines; i++)
	{
		memset(lineF, 0x00, sizeof(lineF));
		strcpy(lineF, list[i]);
		strcpy(lineP, lineF);
		offset = 0;

		//printf("LENDO LINHA %d DE %d - (%ld) [%s]\n", i + 1, lines, strlen(lineF), lineF);

		for (j = 0; j <= PRODUCT_PARAMETERS_COD_FUNCTION; j++)
		{
			size = utl_CSV_FindChar (lineP, ';');
			if (!size)
				size = strlen(lineF) - offset;

			if (j == PRODUCT_PARAMETERS_COD_FUNCTION)
			{
				memset(zAux, 0x00, sizeof(zAux));	
				strncpy(zAux, lineP, size);
				typeActual = atoi(zAux);
				//printf("typeActual = %d\n", typeActual);

				if (typeActual == function)
				{
					strcpy(listOut[k], lineF);
					k++;
				}
			}else
			{
				offset += size + 1;
				strcpy(lineP, &lineF[offset]);
			}
		}
		//printf("LENDO COLUNA %d DA LINHA %d - (%ld) [%s]\n", column, i + 1, strlen(zAux), zAux);		
	}
	
	return k;

}

int utl_CSV_GetListByTypeFunction(int typeFunction, char listOut[][1024])
{
	char list   [512][1024];
	char lineF  [1024] 	= {0x00};
	char lineP  [1024] 	= {0x00};
	char zAux   [1024] 	= {0x00};
	int  lines  		= 0;
	int  offset			= 0;
	int  size 			= 0;
	int  i 				= 0;
	int  j 				= 0;
	int  k				= 0;
	int  typeActual		= 0;

	lines = utl_CSV_ReadToList(CSV_TABLE_PARAMETER_TABLE, list, 512);
	
	for(i = 0; i < lines; i++)
	{
		memset(lineF, 0x00, sizeof(lineF));
		strcpy(lineF, list[i]);
		strcpy(lineP, lineF);
		offset = 0;

		//printf("LENDO LINHA %d DE %d - (%ld) [%s]\n", i + 1, lines, strlen(lineF), lineF);

		for (j = 0; j <= PARAMETER_TABLE_COD_FUNCTION; j++)
		{
			size = utl_CSV_FindChar (lineP, ';');
			if (!size)
				size = strlen(lineF) - offset;

			if (j == PARAMETER_TABLE_COD_FUNCTION)
			{
				memset(zAux, 0x00, sizeof(zAux));	
				strncpy(zAux, lineP, size);
				typeActual = atoi(zAux);
				//printf("typeActual = %d\n", typeActual);

				if (typeActual == typeFunction)
				{
					strcpy(listOut[k], lineF);
					k++;
				}
			}else
			{
				offset += size + 1;
				strcpy(lineP, &lineF[offset]);
			}
		}
		//printf("LENDO COLUNA %d DA LINHA %d - (%ld) [%s]\n", column, i + 1, strlen(zAux), zAux);		
	}
	
	return k;

}

int utl_CSV_JoinProductByBin(long long int bin, int type, char * listOut)
{
	char lineF  [1024] 	= {0x00};
	char lineP  [1024] 	= {0x00};
	char lineC  [1024] 	= {0x00};
	char lineM  [1024] 	= {0x00};
	char xAux   [1024] 	= {0x00};
	char zAux   [1024] 	= {0x00};
	int  offset			= 0;
	int  sizeBin		= 0;
	int  sizeProd		= 0;
	int  prodActualBin	= 0;
	int  prodActualProd	= 0;
	int  i 				= 0;
	int  j 				= 0;
	int  k				= 0;
	int  l				= 0;
	char listBins  [512][1024];
	char listTypes [512][1024];
	int  linesBins 		= 0;
	int  linesTypes		= 0;

	linesBins = utl_CSV_GetListByBin(bin, listBins);
	//printf("linesBins = %d\n", linesBins);
	// for (int i = 0; i < linesBins; i++)
	// 	printf("%s\n", listBins[i]);

	linesTypes = utl_CSV_GetListByType(type, listTypes);
	//printf("linesTypes = %d\n", linesTypes);
	// for (int i = 0; i < linesTypes; i++)
	// 	printf("%s\n", listTypes[i]);

	
	for(i = 0; i < linesBins; i++)
	{
		memset(lineF, 0x00, sizeof(lineF));
		strcpy(lineF, listBins[i]);
		strcpy(lineP, lineF);
		offset = 0;

		//printf("LENDO LINHA %d DE %d - (%ld) [%s]\n", i + 1, linesBins, strlen(lineF), lineF);

		for (j = 0; j <= BIN_TABLE_COD_PRODUCT; j++)
		{
			sizeBin = utl_CSV_FindChar (lineP, ';');
			if (!sizeBin)
				sizeBin = strlen(lineF) - offset;

			if (j == BIN_TABLE_COD_PRODUCT)
			{
				memset(xAux, 0x00, sizeof(xAux));	
				strncpy(xAux, lineP, sizeBin);
				prodActualBin = atoi(xAux);
				//printf("prodActualBin  -> %d\n", prodActualBin);
				//ATÉ AQUI TUDO OK

				for(k = 0; k < linesTypes; k++)
				{
					memset(lineC, 0x00, sizeof(lineC));
					strcpy(lineC, listTypes[k]);
					strcpy(lineM, lineC);
					offset = 0;

					//printf("LENDO LINHA %d DE %d - (%ld) [%s]\n", i + 1, linesTypes, strlen(lineC), lineC);

					for (l = 0; l <= PRODUCTS_TABLE_COD_PRODUCT; l++)
					{
						sizeProd = utl_CSV_FindChar (lineM, ';');
						if (!sizeProd)
							sizeProd = strlen(lineC) - offset;

						if (l == PRODUCTS_TABLE_COD_PRODUCT)
						{
							memset(zAux, 0x00, sizeof(zAux));	
							strncpy(zAux, lineM, sizeProd);
							prodActualProd = atoi(zAux);
							//printf("prodActualBin  -> %d  %d <- prodActualProd\n", prodActualBin, prodActualProd);

							if (prodActualBin == prodActualProd)
							{
								//printf("Reg de Bin%s\n", lineF);
								//printf("Reg de Produto%s\n", lineC);
								memset(zAux, 0x00, sizeof(zAux));	
								sprintf(zAux, "%s;%s", lineF, lineC);
								strcpy(listOut, zAux);
								sizeBin = strlen(listOut);
								return sizeBin;
							}
						}else
						{
							offset += sizeProd + 1;
							strcpy(lineM, &lineC[offset]);
						}
					}
					//printf("LENDO COLUNA %d DA LINHA %d - (%ld) [%s]\n", column, i + 1, strlen(zAux), zAux);		
				}
			}else
			{
				offset += sizeBin + 1;
				strcpy(lineP, &lineF[offset]);
			}
		}
		//printf("LENDO COLUNA %d DA LINHA %d - (%ld) [%s]\n", column, i + 1, strlen(zAux), zAux);		
	}
	
	return k;

}


int utl_CSV_JoinParamByFunction(int typeFuncao, int function, char * listOut)
{
	char lineF  [1024] 	= {0x00};
	char lineP  [1024] 	= {0x00};
	char lineC  [1024] 	= {0x00};
	char lineM  [1024] 	= {0x00};
	char xAux   [1024] 	= {0x00};
	char zAux   [1024] 	= {0x00};
	int  offset			= 0;
	int  sizeFunction		= 0;
	int  sizeTypeFunction		= 0;
	int  actualFunction	= 0;
	int  actualTypeFunction	= 0;
	int  i 				= 0;
	int  j 				= 0;
	int  k				= 0;
	int  l				= 0;
	char listFunctions  [512][1024];
	char listTypesFunction [512][1024];
	int  linesFunctions 		= 0;
	int  linesTyoesFunctions		= 0;

	linesFunctions = utl_CSV_GetListByFunction(function, listFunctions);
	// printf("linesFunctions = %d\n", linesFunctions);
	// for (int i = 0; i < linesFunctions; i++)
	// 	printf("%s\n", listFunctions[i]);

	linesTyoesFunctions = utl_CSV_GetListByTypeFunction(typeFuncao, listTypesFunction);
	// printf("linesTyoesFunctions = %d\n", linesTyoesFunctions);
	// for (int i = 0; i < linesTyoesFunctions; i++)
	// 	printf("%s\n", listTypesFunction[i]);

	
	for(i = 0; i < linesFunctions; i++)
	{
		memset(lineF, 0x00, sizeof(lineF));
		strcpy(lineF, listFunctions[i]);
		strcpy(lineP, lineF);
		offset = 0;

		//printf("LENDO LINHA %d DE %d - (%ld) [%s]\n", i + 1, linesFunctions, strlen(lineF), lineF);

		for (j = 0; j <= PRODUCT_PARAMETERS_COD_PARAMETER; j++)
		{
			sizeFunction = utl_CSV_FindChar (lineP, ';');
			if (!sizeFunction)
				sizeFunction = strlen(lineF) - offset;

			if (j == PRODUCT_PARAMETERS_COD_PARAMETER)
			{
				memset(xAux, 0x00, sizeof(xAux));	
				strncpy(xAux, lineP, sizeFunction);
				actualFunction = atoi(xAux);
				//printf("actualFunction  -> %d\n", actualFunction);
				//ATÉ AQUI TUDO OK

				for(k = 0; k < linesTyoesFunctions; k++)
				{
					memset(lineC, 0x00, sizeof(lineC));
					strcpy(lineC, listTypesFunction[k]);
					strcpy(lineM, lineC);
					offset = 0;

					//printf("LENDO LINHA %d DE %d - (%ld) [%s]\n", i + 1, linesTyoesFunctions, strlen(lineC), lineC);

					for (l = 0; l <= PARAMETER_TABLE_COD_PARAMETER; l++)
					{
						sizeTypeFunction = utl_CSV_FindChar (lineM, ';');
						if (!sizeTypeFunction)
							sizeTypeFunction = strlen(lineC) - offset;

						if (l == PARAMETER_TABLE_COD_PARAMETER)
						{
							memset(zAux, 0x00, sizeof(zAux));	
							strncpy(zAux, lineM, sizeTypeFunction);
							actualTypeFunction = atoi(zAux);
							//printf("actualFunction  -> %d  %d <- actualTypeFunction\n", actualFunction, actualTypeFunction);

							if (actualFunction == actualTypeFunction)
							{
								//printf("Reg de Bin%s\n", lineF);
								//printf("Reg de Produto%s\n", lineC);
								memset(zAux, 0x00, sizeof(zAux));	
								sprintf(zAux, "%s;%s", lineF, lineC);
								strcpy(listOut, zAux);
								sizeFunction = strlen(listOut);
								return sizeFunction;
							}
						}else
						{
							offset += sizeTypeFunction + 1;
							strcpy(lineM, &lineC[offset]);
						}
					}
					//printf("LENDO COLUNA %d DA LINHA %d - (%ld) [%s]\n", column, i + 1, strlen(zAux), zAux);		
				}
			}else
			{
				offset += sizeFunction + 1;
				strcpy(lineP, &lineF[offset]);
			}
		}
		//printf("LENDO COLUNA %d DA LINHA %d - (%ld) [%s]\n", column, i + 1, strlen(zAux), zAux);		
	}
	
	return k;

}


int utl_CSV_GetRegisterByIDs(char * filename, int columnIdA, int idA, int columnIdB, int idB, int columnReg, char * result)
{
	char listA   [512][1024];
	char listB   [512][1024];
	char lineF  [1024] 	= {0x00};
	char lineP  [1024] 	= {0x00};
	char zAux   [1024] 	= {0x00};
	int  linesA  		= 0;
	int  linesB  		= 0;
	int  i				= 0;
	int  j				= 0;
	int  k 				= 0;
	int  l 				= 0;
	int  m 				= 0;
	int  idActual		= 0;
	int  offset			= 0;
	int  size			= 0;

	linesA = utl_CSV_ReadToList(filename, listA, 100);
	
	for(i = 0; i < linesA; i++)
	{
		memset(lineF, 0x00, sizeof(lineF));
		strcpy(lineF, listA[i]);
		strcpy(lineP, lineF);
		offset = 0;

		for (j = 0; j <= columnIdA; j++){

			if (j == columnIdA){
				memset(zAux, 0x00, sizeof(zAux));
				strncpy(zAux, lineP, size);
				idActual = atoi(zAux);

				if (idActual == idA){
					strcpy(listB[k], lineF);
					k++;
					linesB = k;

					for (l = 0; l <= columnIdB; l++){
						size = utl_CSV_FindChar (lineP, ';');
						if (!size)
							size = strlen(lineF) - offset;
						
						if(l == columnIdB){
							memset(zAux, 0x00, sizeof(zAux));
							strncpy(zAux, lineP, size);
							idActual = atoi(zAux);
							if(idActual == idB){
								strcpy(lineP, lineF);
								offset = 0;
								
								for (m = 0; m <= columnReg; m++){
									size = utl_CSV_FindChar (lineP, ';');
									if (!size)
										size = strlen(lineF) - offset;

									if (m == columnReg){
										memset(zAux, 0x00, sizeof(zAux));	
										strncpy(zAux, lineP, size);
										strcpy(result, zAux);
										size = strlen(result);
										return size;
									}else{
										offset += size + 1;
										strcpy(lineP, &lineF[offset]);
									}
								}
								return 0;
							}
						}else{
							offset += size + 1;
							strcpy(lineP, &lineF[offset]);
						}
					}
				}
			}else{
				offset += size + 1;
				strcpy(lineP, &lineF[offset]);
			}
		}
	}
		
	return 0;

}
