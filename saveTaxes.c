
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "defines.h"
#include "utl_CSV.h"

int utl_File_Read(char *filePath, char *output, int position, int portion){

	FILE *pFile;
	int rc = 0;
	char read[1024] = {0x00};

	if(strlen(filePath) < 1) return RC_ERR;

	pFile = fopen(filePath, "r");


	if(pFile == NULL) return RC_ERR;
	if(position > 0) {
		fseek(pFile, position, SEEK_SET);
	}
	if(portion <= 0) {
		portion = 1024;
	}

	rc = fread(read, sizeof(char), portion, pFile);

	strcpy(output, read);

	fclose(pFile);
	
	return rc;
}

int saveTaxes(){
	
    int rc	= RC_OK;
    int ret	= 0;
    int lap	= 0;

	char file[64]	= {0x00};
	char index[64]	= {0x00};
	char tableType[64]	= {0x00};
	char readTable[64]	= {0x00};
	char aux[128]		= {0x00};
	char msg[128]		= {0x00};

	char fields[128]	= {0x00};
	char table[64]		= {0x00};
	char first[32]		= {0x00};
	char second[32]		= {0x00};
	char third[32]		= {0x00};

	remove(CSV_TABLE_INST_TABLE);
	remove(CSV_TABLE_TAX_TABLE);

	do{

		ret = utl_File_Read(TAX_TABLES_PERM, readTable, lap, 16);
		if(ret == RC_ERR) break;
		if(ret == 0) break;
		lap += ret;

		strncpy(tableType, &readTable[0], 3);
		strncpy(index, &readTable[3], 3);
		strncpy(first, &readTable[6], 2);
		strncpy(second, &readTable[8], 5);
		strncpy(third, &readTable[13], 3);

		sprintf(aux, "[%s %s]", tableType, index);

		if(strcmp(tableType, "000") == 0)
			strcpy(file, CSV_TABLE_INST_TABLE);
		else if(strcmp(tableType, "001") == 0)
			strcpy(file, CSV_TABLE_TAX_TABLE);

		memset(table, 0x00, sizeof(table));
		sprintf(table, "%s;%s;%s", first, second, third);
		utl_CSV_insertLine(file, table);

		memset(aux, 0x00, sizeof(aux));
		memset(first, 0x00, sizeof(first));
		memset(second, 0x00, sizeof(second));
		memset(third, 0x00, sizeof(third));
		memset(table, 0x00, sizeof(table));
		memset(readTable, 0x00, sizeof(readTable));
		memset(tableType, 0x00, sizeof(tableType));
		memset(index, 0x00, sizeof(index));

	}while(ret > 0);


    return RC_OK;
}
